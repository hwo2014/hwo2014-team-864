addSbtPlugin("com.github.mpeltonen" % "sbt-idea" % "1.6.0")

addSbtPlugin("org.netbeans.nbsbt" % "nbsbt-plugin" % "1.1.2")

addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.11.2")
