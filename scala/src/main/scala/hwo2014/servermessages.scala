package hwo2014

case class Id(name: String, color: String)

case class Dimensions(length: Double, width: Double, guideFlagPosition: Double)

case class Car(id: Id, dimensions: Dimensions)

case class CarPosition(id: Id, angle: Double, piecePosition: PiecePosition)

case class PiecePosition(pieceIndex: Integer, inPieceDistance: Double, lane: Lane, lap: Integer)

case class Lane(startLaneIndex: Integer, endLaneIndex: Integer)

case class LaneInfo(distanceFromCenter: Integer, index: Integer)

case class ShittyPiece(length: Option[Double], switch: Option[Boolean], radius: Option[Double], angle: Option[Double]) {
  
  def toBetterPiece: TrackPiece = {
    if (length.isDefined) {
      Straight(length.get, switch.getOrElse(false))
    } else if (angle.isDefined) {
      if (angle.get < 0.0) {
        LeftTurn(radius.get, Math.abs(angle.get), switch.getOrElse(false))
      } else {
        RightTurn(radius.get, Math.abs(angle.get), switch.getOrElse(false))
      }
    } else {
      println("Invalid track piece")
      Straight(0.0, false)
    }
    
  }
  
}