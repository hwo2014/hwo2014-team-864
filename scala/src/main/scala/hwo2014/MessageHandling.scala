package hwo2014

import org.json4s.DefaultFormats
import org.json4s.JsonAST.JValue

trait MessageHandling {

  implicit val formats = new DefaultFormats{}
  
  def handleCarPositions(data: JValue) = {
    data.extract[List[CarPosition]]
  }
  
  def handleGameInit(data: JValue) = {
    val track = (data \\ "pieces").extract[List[ShittyPiece]].map(_.toBetterPiece)
    val lanes = (data \\ "lanes").extract[List[LaneInfo]]
    val cars = (data \\ "cars").extract[List[Car]]
    (track, lanes, cars)
  }
  
  def handleYourCar(data: JValue) = {
    data.extract[Id]
  }
  
  def handleDisqualified(data: JValue) = {
    val car = (data \ "car").extract[Id]
    val reason = (data \ "reason").extract[String]
    (car, reason)
  }
  
  def getOurCar(ourCar: Id, cars: List[CarPosition]) = {
    cars.find(_.id == ourCar)
  }
  
  def removeOurCar(ourCar: Id, cars: List[CarPosition]) = {
    cars.filter(_.id != ourCar)
  }

}
