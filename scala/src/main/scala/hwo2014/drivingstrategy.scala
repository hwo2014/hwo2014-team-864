package hwo2014

trait DrivingStrategy {
  
  def calculateThrottle(track: List[TrackPiece], carPosition: CarPosition, otherCars: Seq[CarPosition]): Double = {
    lookAheadTrack(track, carPosition)-lookAheadCars(otherCars, carPosition)
  }
  private def lookAheadTrack(track: List[TrackPiece], carPosition: CarPosition) = {
    track(carPosition.piecePosition.pieceIndex) match {
      case Straight(len, _) => 1.0 - lookAheadNextPiece(track, carPosition.piecePosition.pieceIndex, len-carPosition.piecePosition.inPieceDistance)
      case LeftTurn(radius, angle, _) => (1/(radius-angle))*20
      case RightTurn(radius, angle, _) => (1/(radius-angle))*20
    }
  }
  
  private def lookAheadNextPiece(track: List[TrackPiece], pieceIndex: Integer, stillToGo: Double) = {
    val nextIndex = if (pieceIndex+1 > track.length-1) 0 else pieceIndex+1
    track(nextIndex) match {
      case Straight(_, _) => 1.0
      case LeftTurn(radius, angle, _) => (1/(radius-angle))*10
      case RightTurn(radius, angle, _) => (1/(radius-angle))*10
    }
  }
  
  private def lookAheadCars(otherCar: Seq[CarPosition], carPosition: CarPosition) = {
    0.0 // TODO
  }
  
}