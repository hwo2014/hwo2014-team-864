package hwo2014

sealed abstract class TrackPiece
case class Straight(length: Double, hasSwitch: Boolean) extends TrackPiece
case class RightTurn(radius: Double, angle: Double, hasSwitch: Boolean) extends TrackPiece
case class LeftTurn(radius: Double, angle: Double, hasSwitch: Boolean) extends TrackPiece
