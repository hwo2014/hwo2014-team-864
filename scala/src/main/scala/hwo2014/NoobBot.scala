package hwo2014

import org.json4s._
import org.json4s.DefaultFormats
 import org.json4s.native.JsonMethods._
import java.net.Socket
import java.io.{BufferedReader, InputStreamReader, OutputStreamWriter, PrintWriter}
import org.json4s.native.Serialization
import scala.annotation.tailrec

object NoobBot extends App {
  args.toList match {
    case hostName :: port :: botName :: botKey :: _ =>
      new NoobBot(hostName, Integer.parseInt(port), botName, botKey)
    case _ => println("args missing")
  }
}

class NoobBot(host: String, port: Int, botName: String, botKey: String) extends DrivingStrategy with MessageHandling {
  
  val socket = new Socket(host, port)
  val writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream, "UTF8"))
  val reader = new BufferedReader(new InputStreamReader(socket.getInputStream, "UTF8"))
  
  var track: Option[List[TrackPiece]] = None
  var lanes: Option[List[LaneInfo]] = None
  var cars: Option[List[Car]] = None
  var ourCar: Option[Id] = None
  
  send(MsgWrapper("join", Join(botName, botKey)))
  play

  @tailrec private def play {
    val line = reader.readLine()
    if (line != null) {
      Serialization.read[MsgWrapper](line) match {
        case MsgWrapper("carPositions", data) => {
            val carPositions = handleCarPositions(data)
            val throttle = calculateThrottle(track.get, getOurCar(ourCar.get, carPositions).get, removeOurCar(ourCar.get, carPositions))            
            send(MsgWrapper("throttle", throttle))
        }
        case MsgWrapper("yourCar", data) => {
            val ourCar = handleYourCar(data)
        }
        case MsgWrapper("gameInit", data) => {
            println("Game init")
            val (_track, _lanes, _cars) = handleGameInit(data)
            track = Some(_track)
            lanes = Some(_lanes)
            cars = Some(_cars)                        
        }        
        case MsgWrapper("gameStart", _) => {
            println("Game start!")
        }
        case MsgWrapper("gameEnd", data) => {
            println("Game end!")
            pretty(render(data))
        }
        case MsgWrapper("dnf", data) => {
            val (car, reason) = handleDisqualified(data)
            if (car == ourCar) {
              println("They disqualified us!")
              println("reason: "+reason)
            } else {
              println("reason: "+reason)
              cars = Some(cars.get.filter(_.id != car))
            }
        }
        case MsgWrapper(msgType, _) =>
          println("Received: " + msgType)
      }      
      play
    }
  }

  def send(msg: MsgWrapper) {
    writer.println(Serialization.write(msg))
    writer.flush
  }

}

case class Join(name: String, key: String)
case class MsgWrapper(msgType: String, data: JValue) {
}
object MsgWrapper {
  implicit val formats = new DefaultFormats{}

  def apply(msgType: String, data: Any): MsgWrapper = {
    MsgWrapper(msgType, Extraction.decompose(data))
  }
}
