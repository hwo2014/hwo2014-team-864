package hwo2014

import org.scalatest.FunSpec
import org.json4s.native.Serialization

class MessageTest extends FunSpec with MessageHandling {

  describe("carPositions") {

    it("should handle CarPositions message") {
      val carPositionsMsg = """
      {"msgType": "carPositions", "data": [
  {
    "id": {
      "name": "Schumacher",
      "color": "red"
    },
    "angle": 0.0,
    "piecePosition": {
      "pieceIndex": 0,
      "inPieceDistance": 0.0,
      "lane": {
        "startLaneIndex": 0,
        "endLaneIndex": 0
      },
      "lap": 0
    }
  },
  {
    "id": {
      "name": "Rosberg",
      "color": "blue"
    },
    "angle": 45.0,
    "piecePosition": {
      "pieceIndex": 0,
      "inPieceDistance": 20.0,
      "lane": {
        "startLaneIndex": 1,
        "endLaneIndex": 1
      },
      "lap": 0
    }
  }
], "gameId": "OIUHGERJWEOI", "gameTick": 0}
      """
      Serialization.read[MsgWrapper](carPositionsMsg) match {
        case MsgWrapper("carPositions", data) => {
            handleCarPositions(data)
        }
      }
    }
  }
  
  describe("gameInit") {
    
    it("parses track") {
      val gameInitMsg = """
      {"msgType": "gameInit", "data": {
  "race": {
    "track": {
      "id": "indianapolis",
      "name": "Indianapolis",
      "pieces": [
        {
          "length": 100.0
        },
        {
          "length": 100.0,
          "switch": true
        },
        {
          "radius": 200,
          "angle": 22.5
        }
      ],
      "lanes": [
        {
          "distanceFromCenter": -20,
          "index": 0
        },
        {
          "distanceFromCenter": 0,
          "index": 1
        },
        {
          "distanceFromCenter": 20,
          "index": 2
        }
      ],
      "startingPoint": {
        "position": {
          "x": -340.0,
          "y": -96.0
        },
        "angle": 90.0
      }
    },
    "cars": [
      {
        "id": {
          "name": "Schumacher",
          "color": "red"
        },
        "dimensions": {
          "length": 40.0,
          "width": 20.0,
          "guideFlagPosition": 10.0
        }
      },
      {
        "id": {
          "name": "Rosberg",
          "color": "blue"
        },
        "dimensions": {
          "length": 40.0,
          "width": 20.0,
          "guideFlagPosition": 10.0
        }
      }
    ],
    "raceSession": {
      "laps": 3,
      "maxLapTimeMs": 30000,
      "quickRace": true
    }
  }
}}
      """
      Serialization.read[MsgWrapper](gameInitMsg) match {
        case MsgWrapper("gameInit", data) => {
            handleGameInit(data)
        }
      }
    }
  }
  
  describe("yourCar") {
    it("handles yourCar message") {
      val msg = """
      {"msgType": "yourCar", "data": {
  "name": "Schumacher",
  "color": "red"
}}
      """
      Serialization.read[MsgWrapper](msg) match {
        case MsgWrapper("yourCar", data) => {
            handleYourCar(data)
        }
      }
    }
    
  }

}